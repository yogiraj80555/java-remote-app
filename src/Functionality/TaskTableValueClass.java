/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functionality;

/**
 *
 * @author Gaurav
 */
public class TaskTableValueClass {
    private String process_name;
    private String process_ID;
    
    public TaskTableValueClass(String process,String ID){
        this.process_name = process;
        this.process_ID = ID;    
    }
    
    public String getProcessName(){
        return process_name;
    }
    
    public String getProcessID(){return process_ID; }
}
