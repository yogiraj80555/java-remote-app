/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functionality;

import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author gaurav
 */
public class BackGroundClass {
    public static void listenData(){
        int a= 0;
        while(true){
            try{
            Thread.sleep(15000);

            }catch(Exception e){
                System.out.println("Background Thread error");
            }
             FunctionalityClass fn = new FunctionalityClass();
             Map params =  extractJson(AllPaths.LoginData,fn);
             NetworkConnectionsClass cls = new NetworkConnectionsClass();
             String response = cls.postRequest(AllPaths.BACK_THREAD, "POST", fn.createParams(params),"");
            //in this response contains status so we can id fy what to do with system logoff ahutdown etc,
             System.out.println("\n\n BackGround Method response"+response);
             extractResponse(response,fn);
             //UPDATE `statusTable` SET `status`=3 WHERE userEmail="test@test.com"
        }
        
    }
    
    
    private static Map extractJson(String json, FunctionalityClass fn){
        Map map = new HashMap<>();
        Object obj; 
        try{
           obj = new JSONParser().parse(json);
           JSONObject jobj = (JSONObject)obj;
           map.put("user_email",String.valueOf(jobj.get("user")));
           map.put("physical_addr",fn.getHadrwareAddress());
           map.put("User_task", fn.getUsertask());
        }catch(ParseException e){
            System.out.println("Hey Garry1 json:- "+e.getMessage()+" "+e.getClass());
        }
        return map;
        }
    
    private static void extractResponse(String response,FunctionalityClass fn){
        Object obj;
        try{
            obj = new JSONParser().parse(response);
            JSONObject jobj = (JSONObject)obj;
            int status = Integer.parseInt(String.valueOf(jobj.get("status")));
            int action = Integer.parseInt(String.valueOf(jobj.get("action")));
            actionTaken(status,action,fn);
        }catch(ParseException e){
           System.out.println("Hey Garry1 background class extractResponse json:- "+e.getMessage()+" "+e.getClass()); 
        }catch(NumberFormatException e){
            System.out.println("Hey Garry1 background class extractResponse json:- "+e.getMessage()+" "+e.getClass()); 
        }
    }
    
    private static void actionTaken(int action,int tsk, FunctionalityClass fn){
        if(action == 1){
            fn.shutdownSystem(4);
        }else if(action == 2){
            fn.rebotSystem(4);
        }else if(action == 3){
            fn.signoutSystem(4);
        }else if(action == 4){
            //task killing function
            fn.TaskKiller(tsk);
        }
    }
}
