/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functionality;


import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author gaurav
 */
public class AccountsClass {
    
    public static String registerNew(String... params){
        //maping params for register
        String data = "";
        FunctionalityClass fn = new FunctionalityClass();
        Map<String,String> map = new HashMap<>();
        map.put("mail", params[0]);
        map.put("name", params[1]);
        map.put("pass", params[2]);
        map.put("contact", params[3]);
        map.put("mac",fn.getHadrwareAddress());
        map.put("oper_sys", fn.getOsName());
        map.put("os_user", fn.getUserName());
        map.put("archi", fn.getSystemArchi());
        map.put("os_country", fn.getUserCountry());
        NetworkConnectionsClass nCC = new NetworkConnectionsClass();
        //System.out.println("URL1 is: "+AllPaths.SIGNUP);
        String response = nCC.postRequest(AllPaths.SIGNUP, "POST", fn.createParams(map),"");
        data = getJSONResponse(response);
        return data;
    }
    
    private static String getJSONResponse(String response){
        Object obj;
       String res = "";
        try{
            System.out.println("This is my:- "+response);
            obj = new JSONParser().parse(response);
            JSONObject jobj = (JSONObject)obj;
            res = String.valueOf(jobj.get("response")) ;
        }catch(ParseException e){ System.out.println("Hey Garry1 json:- "+e.getMessage()+" "+e.getClass());}
       
        return res;
    }
    
    public static boolean loginUser(String... params){
        FunctionalityClass fn = new FunctionalityClass();
        String data = "";
        Map<String, String> map = new HashMap<>();
        map.put("u_id", params[0]);
        map.put("pin", params[1]);
        NetworkConnectionsClass ncc = new NetworkConnectionsClass();
        String response =  ncc.postRequest(AllPaths.LOGIN, "POST", fn.createParams(map),"");
        System.out.println(response);
        AllPaths.LoginData = response;
        data = getJSONResponse(response);
        return Boolean.valueOf(data);
    }
}
