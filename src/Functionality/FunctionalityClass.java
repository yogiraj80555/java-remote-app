/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functionality;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import javax.swing.JOptionPane;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author gaurav
 */
public class FunctionalityClass {
    private String osName,userName,osVendor,sysArchi,userCountry,user_dir;
    private Runtime runtime;
    
    public FunctionalityClass(){
        osName = System.getProperty("os.name");
        userName = System.getProperty("user.name");
        osVendor = System.getProperty("java.vendor");
        sysArchi = System.getProperty("os.arch");
        userCountry = System.getProperty("user.country");
        user_dir = System.getProperty("user.dir");
        runtime = Runtime.getRuntime();
        System.out.println("\n\n\n\n"+user_dir);
    }
    
    public String getOsName(){
        return osName;
    }
    public String getUserName(){
        return userName;
    }
    public String getOsVendor(){
        return osVendor;
    }
    public String getSystemArchi(){
        return sysArchi;
    }
    public String getUserCountry(){
        return userCountry;
    }
    
    
    
    
    
    public void captureScreen(int sec){
        CaptureClass.capture(5, user_dir);
    }
    public String getHadrwareAddress(){
        if(this.getOsName().equalsIgnoreCase("linux")){
            return CaptureClass.getMacLinux();
        }else if(this.getOsName().contains("dows"))
        {
            return CaptureClass.getMacWindows();
        }
        return " ";
        
    }
    
    ////////shutdown
    public void shutdownSystem(int sec){
        //check os
        try{
        if(this.getOsName().equalsIgnoreCase("linux")){
            turnofLinux(sec);
        }
        else if(this.getOsName().contains("dows")){
            turnofWindows(sec);
        }
        
        }catch(Exception e){
            System.out.println("Hey Garry :- "+e.getMessage());
        }
    }
    private void turnofLinux(int sec)throws Exception{
        
        Process proc = runtime.exec("shutdown -h -t "+sec);
        System.exit(0);
    }
    private void turnofWindows(int sec)throws IOException{
        Process proc = runtime.exec("shutdown -s -t "+sec);
        System.exit(0);
    }
    
    
    ///////reboot
    public void rebotSystem(int sec){
        try{
            if(this.getOsName().equalsIgnoreCase("linux")){
            rebootLinux(sec);
        }else if(this.getOsName().contains("dows")){
            rebootWindows(sec);
        }
            
        }catch(Exception e){
            System.out.println("Hey Garry :- "+e.getMessage());
        }
    }
    private void rebootLinux(int sec)throws IOException{

        Process proc = runtime.exec("reboot");
        System.exit(0);
    }
    private void rebootWindows(int sec)throws IOException{
        Process proc = runtime.exec("shutdown -r -t "+sec);
        System.exit(0);
    }

    ///////signout
    public void signoutSystem(int sec){
        try{
            if(this.getOsName().equalsIgnoreCase("linux")){
            signoutLinux(sec);
        }else if(this.getOsName().contains("dows")){
            signoutWindows(sec);
        }
            
        }catch(Exception e){
            System.out.println("Hey Garry :- "+e.getMessage());
        }
    }
    private void signoutLinux(int sec)throws IOException{
        //pkill -KILL -u gaurav
        Process proc = runtime.exec("pkill -KILL -u "+this.getUserName());
        System.exit(0);
    }
    private void signoutWindows(int sec)throws IOException{
        Process proc = runtime.exec("shutdown -l");
        System.exit(0);
    }
    
    /////////////////////////////////////////////////////////////////////////
    //for creating parameter for urls 
    protected String createParams(Map<String,String> map){
        StringJoiner js = new StringJoiner("&");
        try{
            for(Map.Entry<String,String> mp :map.entrySet()){
                js.add(URLEncoder.encode(mp.getKey(),"UTF-8")+"="+
                        URLEncoder.encode(mp.getValue(), "UTF-8"));
            }
        }catch(UnsupportedEncodingException e){System.out.println("Hey Garry1:- "+e.getMessage());}
        
        
        return js.toString();
    }
    
    
    ///////////////////////////////////////////////////////////////////////////
    public void CheckAllData(){
        //JOptionPane.showMessageDialog(null, "Please Fill all fields")
        Map params = jsonDecoder(AllPaths.LoginData);
        
        if(!params.get("mac").equals(this.getHadrwareAddress()) && 
                params.get("os_user").equals(this.userName) &&
                params.get("operating_sys").equals(this.osName)){
            String str = "It seems you are using Different\n"
                    + "Interface then previous one";
            JOptionPane.showMessageDialog(null, str);
            //use background to update mac
            
        }
        
        
        if(!params.get("mac").equals(this.getHadrwareAddress()) &&
                !params.get("os_user").equals(this.userName))
        {
            String str = "It seems that you are\nlogining form different system";
            JOptionPane.showMessageDialog(null, str);
            //use background to update everything
        }
        
        if(params.get("mac").equals(this.getHadrwareAddress()) && 
                !params.get("operating_sys").equals(this.osName)){
            String str = "It seems that you are logining form \nDifferent Operating System";
            JOptionPane.showMessageDialog(null, str);
            //use background to update os
        }
        
        Runnable r = new Runnable(){
            public void run(){
              BackGroundClass.listenData();
            }
        
        };
        
        Thread t = new Thread(r);
        t.start();
    }
    
    public Map jsonDecoder(String txt){
        Object obj;
        Map<String,String> map = new HashMap<>();
        try{
            obj = new JSONParser().parse(txt);
            JSONObject jobj = (JSONObject)obj;
            map.put("mac", String.valueOf(jobj.get("py_address")));
            //map.put("user",String.valueOf(jobj.get("user")));
            map.put("operating_sys",String.valueOf(jobj.get("os")));
            map.put("os_user",String.valueOf(jobj.get("os_user")));
            map.put("type",String.valueOf(jobj.get("type")));
            
        }catch(ParseException e){
            System.out.println("Hey Garry1 json:- "+e.getMessage()+" "+e.getClass());
        }
        
        return map;
    }
    
    ///////////////////////////////////////////////////////////////////////////
    //Common method to retrive all data which currently login user
    
    public Map <String, String>login_Data_Decoder(){
        Map<String, String> map = new HashMap<>();
        
        try{
            Object obj = new JSONParser().parse(AllPaths.LoginData);
            JSONObject jObj = (JSONObject)obj;
            map.put("eor",String.valueOf(jObj.get("error")));
            map.put("email",String.valueOf(jObj.get("user")));
            map.put("user_name",String.valueOf(jObj.get("name")));
            map.put("contact", String.valueOf(jObj.get("contact")));
            map.put("o_user",String.valueOf(jObj.get("os_user")));
            map.put("osystem",String.valueOf(jObj.get("os")));
            map.put("permission",String.valueOf(jObj.get("tpe")));
            map.put("response",String.valueOf(jObj.get("response")));
        }catch(ParseException e){
             System.out.println("Hey Garry1 json:- "+e.getMessage()+" "+e.getClass());
        }
        
        
        
        
        return map;
    }
    ////
    
    
    
    
    public String getLogin_Permit(){
        Map<String,String> map = login_Data_Decoder();
        
        return map.get("permission");
    }
    
    
    public void getTitle(javax.swing.JLabel title){
        Map<String,String> map = login_Data_Decoder();
        String name = map.get("user_name");
        title.setText(name);
        System.out.println("  "+map.get("user_name"));
        
    }
    
    
    /////////////////////////////////////////////////////////////////////////////
    
    public ArrayList<TableValueClass> gettingTableData(){
        
        ///send request to get data
        Map<String, String> data = login_Data_Decoder();
        Map<String, String> map = new HashMap<>();
        map.put("email", data.get("email"));
        map.put("name",data.get("user_name"));
        NetworkConnectionsClass NCC = new NetworkConnectionsClass();
        String response = NCC.postRequest(AllPaths.GET_TABLE_DATA, "POST", this.createParams(map),"");
        data = DecodeTableResponse(response);
        System.out.println("Live Data Response is "+data);
        //if some error occurs or no user live then
        try{
        if(Boolean.valueOf((String)data.get("error"))  || Integer.valueOf(data.get("signal")) == 0){
            ArrayList<TableValueClass> lst = new ArrayList<>();
            lst.add(new TableValueClass((String)data.get("response"), "", "", "", "", ""));
            return lst;
        }
        }catch(Exception e){
            System.out.println("There is some error in gettingTableData in functionalityclass "+e.getMessage());
        }
        //if user live and decode data put in to lst and return
        return
        decodeData(data.get("data"));
        
        
    }
    
    /*
    {"error":false,"signal":1,"data":[{"email":"asd@ldg.ada","name":"asda
            asdas","contact":"1928463418","os_user":"gaurav","mac":"70:71:bc:9e:7b:28","os":"Linux"}]}
    
    */
    public Map<String,String> DecodeTableResponse(String str){
        Object obj ;
        Map<String,String> mp = new HashMap<>();
        try{
            obj = new JSONParser().parse(str);
            JSONObject jsonObj = (JSONObject)obj;
            mp.put("error",String.valueOf(jsonObj.get("error")));
            if(Boolean.valueOf(String.valueOf(jsonObj.get("error")))){
                mp.put("response",(String)jsonObj.get("response"));
                return mp;
            }
            mp.put("signal",String.valueOf(jsonObj.get("signal")));
            if(Integer.valueOf(String.valueOf(jsonObj.get("signal"))) == 0){
                
                mp.put("response",(String)jsonObj.get("response"));
                return mp;
            }
            //if no any problem then
            mp.put("data",String.valueOf(jsonObj.get("data")));
        }catch(ParseException e){
           System.out.println("Hey Garry got Exception of json:- "+e.getMessage()+" "+e.getClass()); 
        }
        return mp;
    }
    
    private ArrayList<TableValueClass> decodeData(String data){
    ArrayList<TableValueClass> lst = new ArrayList<>();
    Object obj;
    try{
    obj = new JSONParser().parse(data);
    JSONArray arr = (JSONArray)obj;
    for(int i=0;i<arr.size();i++){
        JSONObject jobj = (JSONObject)arr.get(i);
        lst.add(new TableValueClass(jobj.get("email").toString(), jobj.get("name").toString(),
                jobj.get("contact").toString(), jobj.get("os_user").toString(),
                jobj.get("os").toString(), jobj.get("mac").toString()));
    }
    
    }catch(Exception e){
     System.out.println("Hey Garry got Exception of json:- "+e.getMessage()+" "+e.getClass()+" decodeData Method");
     lst.add(new TableValueClass("", "","", "", "Due to Server Load", " Currently data not Avalible"));
    }
    return lst;
    
    }
    
    /////////////////////////////////////////////////////////////////////////////
    
    
    ////////getting user task
    public String getUsertask(){
        String std = "";
        if(this.osName.equalsIgnoreCase("linux")){
           std = getTaskListofLinux(); 
        }
        else if(this.osName.contains("dows")){
            std = getTaskListofWindows();
        }
        
        return std;
    }
    /////////////////linux get task
    private String getTaskListofLinux(){
        String line= "";
        List<List> lst = new ArrayList<List>();
        try{
            Process p = Runtime.getRuntime().exec("ps -e");
            BufferedReader yes = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while((line = yes.readLine()) != null){
                List<String> s = new ArrayList<String>();
                line = removeExtraSpace(line);
                String[] l = line.split(" ");
                s.add(l[0]);s.add(l[3]);
                lst.add(s);
            }
        }catch(Exception e){
        
        }
        lst.remove(0);
        return lst.toString();
    }
    
    
    private String removeExtraSpace(String line)
    {
        if (line.length()<1){return null;}
        line = line.trim();
        char a[] = line.toCharArray();
        String str = "";
        boolean flg = false;
        for (int i=0;i<a.length-1;i++){
            if(a[i] != ' ' || a[i+1] != ' '){
                str += a[i];
            }
        }
        str += a[a.length-1];
        return str;
    }
    ////////////////////////////////linux get task end
    
    ////////////////////////////////windows get task
    private String getTaskListofWindows(){
        String line= "";
        List<List> lst = new ArrayList<List>();
        try{
            Process p = Runtime.getRuntime().exec(System.getenv("windir") +"\\system32\\"+"tasklist.exe");
            BufferedReader yes = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while((line = yes.readLine()) != null){             
                List<String> s = new ArrayList<String>();
                //System.out.println("This is: "+line);
                line = removeExtraSpace(line);
                if (line == null){continue;}
                String[] l = line.split(" ");
                s.add(l[0]);s.add(l[1]);
                lst.add(s);
            }
        }catch(Exception e){
            System.out.println("Got an exception is windows task getting "+e.getMessage());
        }
        
        System.out.println("List size is: "+lst.size());
        
        if (lst.size() > 3){
            lst.remove(0);lst.remove(0);lst.remove(0);//remove top 3
        }
        
        return lst.toString();
    }
    
    //for removing extra space windows get task uses linux removeExtraSpace function
    
    ////////////////////////////////windows get task end
    
    
    /////////////////////////////////////////////sending operational code
    public boolean getCodes(int a,String email){
        Map<String,String> map = new HashMap<String,String>();
        map.put("Email", email.trim().toString());
        map.put("operationalCode", String.valueOf(a));
        NetworkConnectionsClass cls = new NetworkConnectionsClass();
        String response = cls.postRequest(AllPaths.SEND_CODES, "POST",this.createParams(map),"");
        System.out.println("Code Response is :"+response);
        return getResponseOfCodes(response);
    }
    
    private boolean getResponseOfCodes(String response){
        Object obj;
        try{
            obj = new JSONParser().parse(response);
            JSONObject j = (JSONObject)obj;
            if(!Boolean.valueOf(j.get("error").toString())  && Boolean.valueOf(j.get("response").toString())){
              return true;  
            }
            
            return false;
        
        }catch(ParseException e){
            System.out.println("Hey Garry This is exception form getResponseofCodes function "+e.getMessage());
        }
        
        return false;
    }
    ////////////////////////////////////operational code ends

    
    ///////////////////////////////////get all task list from user
    public ArrayList<TaskTableValueClass> getTaskList(){
        ArrayList<TaskTableValueClass> data = new ArrayList<>(); //hear we assign because if any exception raise we have to deal with it 
        try{
        Map params = new HashMap<>();
        params.put("mail", AllPaths.CurrentSeletedEmail);
        params.put("condition",AllPaths.CurrentSeletedContact);
        NetworkConnectionsClass nw = new NetworkConnectionsClass();
        String response = nw.postRequest(AllPaths.GET_USER_TASK, "POST", this.createParams(params),"");
        data = decodeResponse(response);
        //System.out.println("My Response is: "+response);
        return data;
        }catch(Exception e){
            System.out.println("Hey Garry Exception Occurs In getTaskList of functionality class "+e.getMessage());
        }
        data.add(new TaskTableValueClass("Sorry Data Not Avalible", "Try again Somtime"));
        return data;
    }
    
    private ArrayList<TaskTableValueClass> decodeResponse(String response){
        ArrayList<TaskTableValueClass> data = new ArrayList<>();
        Object obj;
        try{
            
            obj = new JSONParser().parse(response);
            JSONObject jobj = (JSONObject)obj;
            if(Boolean.getBoolean(jobj.get("error").toString()) || Integer.parseInt(jobj.get("signal").toString()) == 0){
                data.add(new TaskTableValueClass("Sorry Data Not Avalible", "Try again Somtime"));
                
            }else{
            //good to go
                
                String s = jobj.get("response").toString().replaceAll("\\[", "");
                s = s.replaceAll("\\]", "");
                System.out.println("I came hear");
                String[] x = s.split(",");
                for(int i=0;i<x.length;i+=2){
                    data.add(new TaskTableValueClass(x[i], x[i+1]));
                }
            }
            return data;
        }catch(Exception e){
        System.out.println("Caught an Exception while decoding json of user task "+e.getMessage());
        }
        data.add(new TaskTableValueClass("Sorry Data Not Avalible", "Try again Somtime"));
        return data;
    }
    /////////////////////////////////// End get all task list from user
    
    
    ///////////////////////////////// task id send to kill
    public void taskIDSending(String id){
        System.out.println("The Task Id is:"+id);
        try{
            Map params = new HashMap<>();
            params.put("mail", AllPaths.CurrentSeletedEmail.trim());
            params.put("condition",id);
            NetworkConnectionsClass nc = new NetworkConnectionsClass();
            String response = nc.postRequest(AllPaths.SET_FOR_USER_TASK, "POST", this.createParams(params),"");
            
            
            
        }catch(Exception e){
            System.out.println("Hay Garry Exception is occurs in Task ID Sending "+e.getMessage());
        }
    }
    ///////////////////////////////// task id send to kill  END
    
    ////////////////////////////////Task Killer
    public void TaskKiller(int id){
        try{
        if(this.osName.equals("linux")){
            //kill linux task
            stopLinuxTask(id);
        }
        else if(this.osName.contains("dows")){
            stopWindowsTask(id);
        }
        
        }catch(Exception e){
            System.out.println("Hey Garry Exception is occurs during task Killing Please check once "+e.getMessage());
        }
    }
    
    
    public void stopLinuxTask(int id){
        String line,s = "";
        try{
        Process p = Runtime.getRuntime().exec("kill "+id);
        BufferedReader yes = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while((line = yes.readLine()) != null){
               s += line;
            }
        System.out.println("This is Executing Linux Task ->"+s);

        }catch(IOException e){
            System.out.println("Hey Garray Exception occurs is StopLinuxTask "+e.getMessage());
        }
    }
    
    public void stopWindowsTask(int id){
        //taskkill /F /PID pid_number
        String line,s = "";
        try{
        Process p = Runtime.getRuntime().exec("taskkill /F /PID "+id);
        BufferedReader yes = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while((line = yes.readLine()) != null){
               s += line;
            }
        System.out.println("This is Executing Windows Task ->"+s);

        }catch(IOException e){
            System.out.println("Hey Garray Exception occurs is StopWindowsTask "+e.getMessage());
        }
    }
    ////////////////////////////////Task Killer end
    
    
    
    
    
    ///////////////////detect to user inactivity
    
    //remain 
    //detect all  event like form keybord onkeypressed 
    //and for mouse MOUSE_MOVED,MOUSE_CLICKED,MOUSE_PRESSED,MOUSE_RELEASED among them first tow event are best
    /////////////////////////////////////////////
    
    
}
