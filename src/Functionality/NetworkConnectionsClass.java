/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functionality;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 *
 * @author gaurav
 */
public class NetworkConnectionsClass {
    URL url;
    HttpURLConnection httpConnection;
    /* URL Method, and params and get result in string */
    protected String postRequest(String path,String mtd,String... params){
//        System.out.println("Url is:- "+path+
//                "\nURL is:-http://192.168.0.200/javaRemoteapp/signup.php"+
//                "\nMethod is:- "+mtd+"\nParams:- "+params[0]);
        try{
            url = new URL(path);
        }catch(MalformedURLException e){
            System.out.println("Hey Garry11 NetworkConnectionsClass MalformedURLException:- "+e.getMessage()+this.getClass().toString());
        }
        
        try{
            httpConnection = (HttpURLConnection)url.openConnection();
        }catch(IOException e){System.out.println("Hey Garry22 NetworkConnectionsClass:- "+e.getMessage());}
        
        
        try{
            httpConnection.setRequestMethod(mtd);
        }catch(ProtocolException e){System.out.println("Hey Garry33 NetworkConnectionsClass:- "+e.getMessage());}
        
        httpConnection.setRequestProperty("Accept", "application/json");
        httpConnection.setReadTimeout(30000);
        httpConnection.setDoInput(true);
        httpConnection.setDoOutput(true);
        
        
        try{
            try(OutputStream os = httpConnection.getOutputStream()){
                byte[] input = params[0].getBytes("utf-8");
                os.write(input,0,input.length);
            }
        }catch(IOException e){System.out.println("Hey Garry44 NetworkConnectionsClass:- "+e.getMessage());}
        
        
        
        try{
            
            try(BufferedReader br = new BufferedReader(
            new InputStreamReader(httpConnection.getInputStream(),"utf-8"))){
                StringBuilder sb = new StringBuilder();
                String respLine = null;
                while((respLine = br.readLine()) != null){
                    sb.append(respLine.trim());
                }
                return sb.toString();
            }
            
        }catch(IOException e){System.out.println("Hey Garry55 NetworkConnectionsClass:- "+e.getMessage());}        
        
        httpConnection.disconnect();
        
        return params[1].toString();
    }
}
