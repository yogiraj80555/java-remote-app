
package Functionality;
import javax.swing.table.AbstractTableModel;
public class TaskTableModelClass extends AbstractTableModel {
    private String[] columnName;
    private Object[][] rowsValue;
    
    public TaskTableModelClass(){}//best practice to do this
    
    
    public TaskTableModelClass(String[] colStrings,Object[][] rows){
        this.columnName = colStrings;
        this.rowsValue = rows;
    }
    
    public Class getColumnClass(int col){
        return getValueAt(0,col).getClass();
    }
    
    
    @Override
    public int getRowCount() {
        return this.rowsValue.length;
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return this.rowsValue[rowIndex][columnIndex];
    }
    
    
    @Override
    public String getColumnName(int col){
        return this.columnName[col];
    }
    
}
