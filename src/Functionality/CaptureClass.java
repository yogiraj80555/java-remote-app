/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functionality;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import javax.imageio.ImageIO;

/**
 *
 * @author gaurav
 */
public class CaptureClass {
    
    public static void capture(int sec,String path){
        try{
            
        Robot r = new Robot();
        Rectangle ract = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        BufferedImage img = r.createScreenCapture(ract);
        ImageIO.write(img,"jpg",new File(path));
        }catch(AWTException | IOException  e)
        {
            System.out.print("Hey Garry :- "+e.getMessage());
        }
    }
    
    protected static String getMacWindows(){
        InetAddress ia;
        try{
            ia = InetAddress.getLocalHost();
            
            
            System.out.println("Ip address: "+ia.getHostAddress()
                    +"\n Host: "+ia.getHostName()+" -- "+ia.getCanonicalHostName()+
                    " -- "+ia.getAddress().toString());
          
            
            
            NetworkInterface interfaces = NetworkInterface.getByInetAddress(ia);
            byte[] mac = interfaces.getHardwareAddress();
            StringBuilder sb = new StringBuilder();
            for(int i=0;i<mac.length;i++){
                sb.append(String.format("%02x%s", mac[i],(i < mac.length-1)?":":""));
            }
            System.out.println("Display Name: "+interfaces.getDisplayName()+
                    "\nInterface Add Address: "+interfaces.getInterfaceAddresses()+
                    "\nMTU: "+interfaces.getMTU()+
                    "\nParent: "+interfaces.getParent()+
                    "\nSub Interface"+interfaces.getSubInterfaces()+
                    "\nInet Addresses: "+interfaces.getInetAddresses()+
                    "\nHash code"+interfaces.hashCode());
            
            
            return sb.toString();
        }catch(UnknownHostException e){
            System.out.print("Hay Garry:- "+e.getMessage()+" Custome:-Host Not Found");
        }catch(SocketException e){
            System.out.print("Hey Garry:- "+e.getMessage());
        }
        return null;
    }
    
    protected static String getMacLinux() {
        try{
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();

        while (interfaces.hasMoreElements())
        {
        NetworkInterface nif = interfaces.nextElement();      
        byte[] mac = nif.getHardwareAddress();
            StringBuilder sb = new StringBuilder();
            for(int i=0;i<mac.length;i++){
                sb.append(String.format("%02x%s", mac[i],(i < mac.length-1)?":":""));
            }
            System.out.println("Display Name: "+nif.getDisplayName()+
                    "\nInterface Add Address: "+nif.getInterfaceAddresses()+
                    "\nMTU: "+nif.getMTU()+
                    "\nParent: "+nif.getParent()+
                    "\nSub Interface"+nif.getSubInterfaces()+
                    "\nInet Addresses: "+nif.getInetAddresses()+
                    "\nHash code"+nif.hashCode());
            
            System.out.println(sb.toString());    
            return sb.toString();
        }
        }catch(SocketException e){}
    return null;
    }
    
}
