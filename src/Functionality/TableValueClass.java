/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functionality;

/**
 *
 * @author gaurav
 */
public class TableValueClass {
    private String email;
    private String user_name, contact, OS_user, OSystem, macID;

    public TableValueClass(String email, String user_name, String contact, String OS_user, String OSystem, String macID) {
        this.email = email;
        this.user_name = user_name;
        this.contact = contact;
        this.OS_user = OS_user;
        this.OSystem = OSystem;
        this.macID = macID;
    }

    public String getEmail() {
        return email;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getContact() {
        return contact;
    }

    public String getOS_user() {
        return OS_user;
    }

    public String getOSystem() {
        return OSystem;
    }

    public String getMacID() {
        return macID;
    }
    
    
}
