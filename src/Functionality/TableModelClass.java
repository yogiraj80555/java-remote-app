                                                                                                                                            /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functionality;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author gaurav
 */
public class TableModelClass extends AbstractTableModel{
     private String[] colums;
     private Object[][] rows;
     
     
    public TableModelClass(){}//best practice to to this 
     
    public TableModelClass(String[] cols, Object[][] data){
        this.colums = cols;
        this.rows = data;
        
    }
    
    
    
    public Class getColumsClass(int cols){
        //place if here and return image when image inserted
        
        return getValueAt(0,cols).getClass();
    }

    @Override
    public int getRowCount() {
        return this.rows.length;
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getColumnCount() {
        return this.colums.length;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
       
        return this.rows[arg0][arg1];

            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
     @Override
    public String getColumnName(int col){
        return this.colums[col];
    }
    
}
