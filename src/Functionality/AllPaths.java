/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Functionality;

/**
 *
 * @author gaurav
 */
public class AllPaths {
    private static final String MAIN_URL = "https://yogiraj.coolpage.biz";
    //private static final String MAIN_URL= "http://182.168.10.200";
    private static final String BASE_URL = MAIN_URL+"/javaRemoteapp";
    public static final String LOGIN = BASE_URL+"/login.php"; 
    public static final String SIGNUP = BASE_URL+"/signup.php";
    public static final String BACK_THREAD = BASE_URL+"/backThread.php";
    public static final String GET_TABLE_DATA = BASE_URL+"/getAllLive.php";
    public static final String SEND_CODES = BASE_URL+"/operations.php";
    public static final String GET_USER_TASK = BASE_URL+"/getTask.php";
    public static final String SET_FOR_USER_TASK = BASE_URL+"/setTask.php";
    public static String LoginData; //store login data
    public static String CurrentSeletedEmail,CurrentSeletedContact; //store current seleted details
}
