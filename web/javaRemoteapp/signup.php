<?php

require_once 'includes/Dbo.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){

	if(isset($_POST['mail']) and isset($_POST['name']) and isset($_POST['pass']) and isset($_POST['contact']) and isset($_POST['mac']) and isset($_POST['oper_sys']) and isset($_POST['os_user']) and isset($_POST['archi']) and isset($_POST['os_country'])){

		$db = new Dbo();
		if(strlen($_POST['contact'])<11 && strlen($_POST['contact'])>9 ){

			if(!($db->checkUser($_POST['mail'],$_POST['contact']))){

					if($db->createUser($_POST['mail'],ucfirst($_POST['name']),$_POST['pass'],$_POST['contact'],
						$_POST['mac'],$_POST['oper_sys'],$_POST['os_user'],$_POST['archi'],$_POST['os_country'])) {
						$data["error"] = false;
						$data["response"] = "User Register Successfully ";	
					}else{
						$data["error"] = true;
						$data["response"] = "There is Some Problem with User Registration \n \tTry again Later";	
					}					

			}else{	
				$data["error"] = true;
				$data["response"] = "User Already Registerd with us. \nPlease Contact System Admin.\nThankyou.";
			}
		}else{
			$data["error"] = true;
			$data["response"] = "Enter 10 digit Indian Mobile Contact Number only";
		}

	}else{
		$data["error"] = true;
		$data["response"] = "Required Fields are Missing";
	}

	echo json_encode($data);
}else{
	$var["error"] = true;
	$var["response"] = "Server is Busy Try Again Sometime";
	sleep(39);
	header("Location: http://www.google.com/"); 
	echo json_encode($var);
	exit();
}

?>