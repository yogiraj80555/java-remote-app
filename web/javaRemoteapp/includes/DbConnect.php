<?php

class DbConnect{
	private $con;

	function connect(){
		include_once dirname (__FILE__).'/constant.php';
		$this->con = mysqli_connect(DB_HOST, DB_USER, DB_PASS,DB_NAME);
		if(mysqli_connect_errno()){
			echo "Failed to Connect Database".mysqli_connect_errno();
		}

		return $this->con;
	}
}

?>
