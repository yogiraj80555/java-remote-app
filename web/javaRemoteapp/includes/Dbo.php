<?php

class Dbo{
	private $con;

	function __construct(){
		require_once dirname(__FILE__).'/DbConnect.php';
		$db = new DbConnect();
		$this->con = $db->connect();
	}

	function close(){
		mysqli_close($this->con);
	}

	function userLogin($user,$pass){
		$query = "select * from users where email='$user' and password='$pass'";
		$result = mysqli_query($this->con,$query);
		$data = mysqli_num_rows($result)>0; 
		
		return $data;
	}

	function getUser($user){
		$query = "select * from users where email='$user' ";
		$result = mysqli_query($this->con,$query);
		$data = mysqli_fetch_row($result);
		
		return $data;
	}


	function checkUser($mail,$contacts){
		$query = "select * from users where contact='$contacts' OR email='$mail' ";
		$result = mysqli_query($this->con,$query);
		$data =  mysqli_num_rows($result)>0;
		//echo $data;
	
		return $data;
	}


	function createUser($mail,$name,$pass,$contact,$mac,$o_systems,$o_user,$s_archi,$o_cntry,$tpe="user"){
		
		$query = "insert into users (email, password, full_name, contact, pro_picture, mac_address, o_system, os_user_name, sys_archi, user_country, type) values('$mail','$pass','$name','$contact','C:/user/','$mac','$o_systems','$o_user','$s_archi','$o_cntry','$tpe')";
		$data = mysqli_query($this->con,$query);
		if($data){
		$query = "insert into statusTable (userEmail, currentTime, status) values('$mail','','0')";
		$data = mysqli_query($this->con,$query);
		return $data;
		}
		
		return $data;
	}



	function backThreadWork($email,$mac,$tasklst){
		$time = date("Y-m-d H:i:s");
		$query = "select * from statusTable where userEmail='$email'";
		$data = mysqli_query($this->con,$query);
		$result = mysqli_fetch_row($data);
		//echo '<pre>';
		//print_r($result);
		//reading the file
		/*
		$fp = fopen("./taskfiles/".$email.".txt","r");
		echo fread($fp,filesize("./taskfiles/".$email.".txt"));
		fclose($fp);
		*/
		$this->writefile($email,$tasklst); //writing current running task to file
		if($result[3] == 0 || $result[3] == 5){//this 5 is for capture screen
			//update current time 
			$query = "update statusTable set currentTime='$time' where userEmail='$email'";
			$data = mysqli_query($this->con,$query);
			return $result;
		}else{
			//update and result
			//echo "done";
			$query = "update statusTable set status=0 where userEmail='$email'";
			$data = mysqli_query($this->con,$query);
			return $result;
		}
	}


	private function writefile($email,$tasklst){
		$fp = fopen("./taskfiles" . "/".$email.".class","wb");
		fwrite($fp,$tasklst);
		fclose($fp);
	}



	function validateAdmin($email,$name){
		$query = "select type from users where email='$email' and full_name='$name'";
		$data = mysqli_query($this->con,$query);
		$result = mysqli_fetch_row($data);
		if($result[0] == "adam"){
			return true;
		}
		return false;
	}


	function getAllLive(){
		//$result = array();
		$time = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s"))-20);
		//print_r($time)	;
		$query = "select userEmail from statusTable where currentTime >= '$time'";
		$data = mysqli_query($this->con,$query);
		$result = mysqli_fetch_all($data,MYSQLI_NUM);//MYSQLI_ASSOC
		//echo "<pre>";
		//$d = $result[0];
		//print_r($d);
		//print_r(count($result));
		//print_r( "Arrayis: ".empty($result)." already");
		//$result get all the email
		if(!empty($result)){
			//array not empty return all email data
			$cnt = count($result);
			$arr = array();
			foreach ($result as $as => $email){
				$data = $this->getUser($email[0]);
				$field['email'] = $data[1];
				$field['name'] = $data[3];
				$field['contact'] = $data[4];
				$field['os_user'] = $data[8];
				$field['mac'] = $data[6];
				$field['os'] = $data[7];
				array_push($arr,$field);

			}
			//echo "<pre>";
			//print_r($arr);
			return $arr;
		}else{
			//array is an empty
			return false;
		}
	}


	function modifyOperationalCodes($mail,$code){
		//print_r("Code is ".$code);
		$query = "update statusTable set status='$code' where userEmail='$mail'";
		$data = mysqli_query($this->con,$query);
		return $data;
	}



	function getThreadsList($mail,$condition){
		if (file_exists("./taskfiles/".$mail.".class")){
			//then file exists so read it
			$file = fopen("./taskfiles/".$mail.".class",'r')or die("Unable to open file");
			$data = fread($file,filesize("./taskfiles/".$mail.".class"));
			return $data;
		}
		return false;
	}


	function UpdateTask($mail,$taskno){
		$query = "update statustable set status=4, tnumber='$taskno' where userEmail='$mail'";
		$data = mysqli_query($this->con,$query); 
		return $data;
	}
	
}


?>