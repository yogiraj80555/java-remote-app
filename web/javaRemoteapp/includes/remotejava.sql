-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2020 at 04:13 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `remotejava`
--

-- --------------------------------------------------------

--
-- Table structure for table `statustable`
--

CREATE TABLE `statustable` (
  `id` int(11) NOT NULL,
  `userEmail` text NOT NULL,
  `currentTime` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `tnumber` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statustable`
--

INSERT INTO `statustable` (`id`, `userEmail`, `currentTime`, `status`, `tnumber`) VALUES
(1, 'asdas@asd.asdas', '0000-00-00 00:00:00', 0, 0),
(2, 'asd@ldg.ada', '2019-12-05 14:46:55', 3, 0),
(3, 'test@test.com', '2020-02-16 07:03:55', 0, 5056),
(4, 'pqr@xyz.com', '2020-02-16 06:47:37', 0, 5016),
(5, 'qwe@qwe.qwe', '2020-02-10 02:40:06', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `full_name` text NOT NULL,
  `contact` text NOT NULL,
  `pro_picture` text NOT NULL,
  `mac_address` text NOT NULL,
  `o_system` text NOT NULL,
  `os_user_name` text NOT NULL,
  `sys_archi` text NOT NULL,
  `user_country` text NOT NULL,
  `type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `full_name`, `contact`, `pro_picture`, `mac_address`, `o_system`, `os_user_name`, `sys_archi`, `user_country`, `type`) VALUES
(2, 'test@example.com', '@12345$#^', 'testing user checking', '8520258521', 'C:/user/', '90:87:95:12:85:e3', 'Linux', 'Gaurav', 'amd64', 'IN', 'user'),
(12, 'pqr@xyz.com', '1234512345', 'pqr xyz', '1234432114', 'C:/user/', '70:71:bc:9e:7b:28', 'Linux', 'gaurav', 'amd64', 'IN', 'user'),
(16, 'ASDAS@sf.sdfs', '1234554321', 'sdfsd sdfvsd', '1234554321', 'C:/user/', '70:71:bc:9e:7b:28', 'Linux', 'gaurav', 'amd64', 'IN', 'user'),
(18, 'asd@ldg.ada', '1234554321', 'asda asdas', '1928463418', 'C:/user/', '70:71:bc:9e:7b:28', 'Linux', 'gaurav', 'amd64', 'IN', 'user'),
(19, 'test@test.com', '1234554321', 'test user', '8055593539', 'C:/user/', '70:71:bc:9e:7b:28', 'Linux', 'gaurav', 'amd64', 'IN', 'adam'),
(20, 'qwe@qwe.qwe', '12341234', 'Qwe ert', '0003336669', 'C:/user/', '70:71:bc:9e:7b:28', 'Linux', 'gaurav', 'amd64', 'IN', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `statustable`
--
ALTER TABLE `statustable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `statustable`
--
ALTER TABLE `statustable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
