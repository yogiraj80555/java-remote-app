<?php
require_once 'includes/Dbo.php';

if($_SERVER['REQUEST_METHOD'] == "POST"){
	$db = new Dbo();
	if(isset($_POST['mail']) and isset($_POST['condition'])){

		$response = $db->getThreadsList($_POST['mail'],$_POST['condition']);
		//echo $response;
		$data['error'] = false;

		if ($response == false){
			$data['signal'] = 0;
			$data['response'] = "Currently Data Not Avalible for user";
		}else{
			
			$data['response'] = $response;//base64_encode($response);
			$data['signal'] = 1;
		}
		
	}else{
		$data["error"] = true;
 		$data["response"] = "Required Fields are Missing";
	}

	echo json_encode($data);

}else{

	$var["error"] = true;
	$var["response"] = "Server is Busy Try Again Sometime";
	sleep(39);
	header("Location: http://www.google.com/"); 
	//	echo json_encode($var);
	exit();
}


?>