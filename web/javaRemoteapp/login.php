<?php
require_once 'includes/Dbo.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){

	if(isset($_POST['u_id']) and isset($_POST['pin'])){

		$db = new Dbo();

		if($db->userLogin($_POST['u_id'],$_POST['pin'])){

			$user = $db->getUser($_POST['u_id']);
			//echo "".$user;
			$data['error'] = false;
			$data['user'] = $user[1];
			$data['name'] = $user[3];
			$data['contact'] = $user[4];
			$data['py_address'] = $user[6];
			$data['os'] = $user[7];
			$data['os_user'] = $user[8];
			$data['tpe'] = $user[11];
			$data['response'] = true;

			//$data['data'] = $user;

		}else{
			$data['error'] = true;
			$data['response'] = "Invalid UserName OR Password";
		}
	}else{
		$data['error'] = true;
		$data['response'] = "Required Fields are Missing";
	}

	
	echo json_encode($data);

}else{
$var["error"] = true;
$var["response"] = "Server is Busy Try Again Sometime";
sleep(39);
header("Location: http://www.google.com/"); 
echo json_encode($var);
exit();

}
?>

