<?php
	require_once 'includes/Dbo.php';

	if($_SERVER['REQUEST_METHOD'] == "POST"){

		$db = new Dbo();
		if(isset($_POST['mail']) and isset($_POST['condition'])){

			$response = $db->UpdateTask($_POST['mail'],$_POST['condition']);
			print_r($response); 
			if($response == true){
				$data['error'] = false;
				$data['response'] = true;
			}else{
				$data['error'] = true;
			}

		}else{

			$data["error"] = true;
 			$data["response"] = "Required Fields are Missing";
		}

		echo json_encode($data);
	}else{
		
		$var["error"] = true;
		$var["response"] = "Server is Busy Try Again Sometime";
		sleep(39);
		header("Location: http://www.google.com/"); 
		echo json_encode($var);
		exit();
	}

?>