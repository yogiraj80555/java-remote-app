# JAVA Remote App
This Application is created for remote controlling Computer which is supporetd for Linux as well as windows system requiredment is JDK8.1 to run this application.

This Application and its code is freely use avalible and user are able to integrate this code in their application with prior notification via email etc...

Couple of snaps of application:
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<figure class="figure mb-3">
  <img src="http://www.patilyogiraj.ml/OtherWork/javaremoteapp/login.PNG" width="400" height="380" class="figure-img img-fluid rounded" alt="User's Login Screen.">
  <figcaption class="figure-caption"><b><i>Login Screen </b></i></figcaption>
</figure>

<br/>


<figure class="figure mt-4 mb-3">
  <img src="http://www.patilyogiraj.ml/OtherWork/javaremoteapp/signup.PNG" width="400" height="380" class="figure-img img-fluid rounded" alt="User's Sign Up.">
  <figcaption class="figure-caption"><b><i>Sign Up </b></i></figcaption>
</figure>
<br/>
<figure class="figure mt-4 mb-3">
  <img src="http://www.patilyogiraj.ml/OtherWork/javaremoteapp/dashboard.PNG" width="560" height="380" class="figure-img img-fluid rounded" alt="User's Application Admin Dash.">
  <figcaption class="figure-caption"><b><i>Application Admin Dash </b></i></figcaption>
</figure>
<br/>
<figure class="figure mt-4 mb-3">
  <img src="http://www.patilyogiraj.ml/OtherWork/javaremoteapp/usertasks.PNG" width="400" height="380" class="figure-img img-fluid rounded" alt="User's  Running Tasks.">
  <figcaption class="figure-caption"><b><i>User's Running Tasks </b></i></figcaption>
</figure>
<br/>
<br/>








<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script><br/>

For any query, questions and suggestions please feel free to contact on below mail id
also any advancement is appriciated.<br/>
Thankyou. <br/>
By Yogiraj Patil <br/>
Mail: yogiraj.218m0064@viit.ac.in